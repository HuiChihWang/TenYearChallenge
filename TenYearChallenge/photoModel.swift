//
//  photoModel.swift
//  TenYearChallenge
//
//  Created by Hui Chih Wang on 2021/3/26.
//

import Foundation
import UIKit

struct Photo {
    var photoName = ""
    var photoDesc = ""
    var photoYear = 1990
    let image: UIImage?
    
    init(name: String, description: String, year: Int) {
        photoName = name
        photoDesc = description
        photoYear = year
        image = UIImage(named: name)
    }
}

class YearsPhoto {
    var photos = [Int: Photo]()
    
    public func addPhoto(photo: Photo) {
        photos[photo.photoYear] = photo
    }
    
    public func getPhotoWithYear(year: Int) -> Photo? {
        return photos[year]
    }
}


func createSamples() -> YearsPhoto {
    let yearPhotos = YearsPhoto()
    
    let photoDescription: [String: String] = [
        "2010": "巨嬰寶寶",
        "2011": "瘦了一點",
        "2012": "屁孩染頭髮",
        "2013": "二胡王子",
        "2014": "芭達雅的色情畢旅",
        "2015": "等火車回台中",
        "2016": "這個頭髮很歐爸",
        "2017": "環球影城的歐爸",
        "2018": "當兵都在拍網帥照",
        "2019": "來到歐爸的國家",
        "2020": "人比花嬌",
        "2021": "究極進化",
    ]
    
    for (year, description) in photoDescription {
        let photo = Photo(name: year, description: description, year: Int(year)!)
        yearPhotos.addPhoto(photo: photo)
    }
    
    return yearPhotos
}
