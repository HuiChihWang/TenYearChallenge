//
//  ViewController.swift
//  TenYearChallenge
//
//  Created by Hui Chih Wang on 2021/3/25.
//

import UIKit

class PhotoViewController: UIViewController {


    @IBOutlet weak var currentImage: UIImageView!
    @IBOutlet weak var currentYearLabel: UILabel!
    @IBOutlet weak var yearSlider: UISlider!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var playLabel: UILabel!
    
    let yearPhotos = createSamples()
    let dateFormatter = DateFormatter()
    var currentYear: Int = 2015
    var timer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //adjust view prperty
        dateFormatter.dateFormat = "yyyy-MM-dd"
        currentImage.layer.cornerRadius = 20
        datePicker.setValue(UIColor.white, forKey: "textColor")
        rotateYearLabel()
        
        //set initial view
        updatePhotoView(year: currentYear)
        updateYearView(year: currentYear)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        timer.invalidate()
    }
    
    func rotateYearLabel() {
        currentYearLabel.layer.cornerRadius = 5
        currentYearLabel.layer.masksToBounds = true
        let transform = CGAffineTransform(rotationAngle: -.pi / 6)
        currentYearLabel.transform = transform
    }
    
    func convertDateToInstance(year: Int, month: Int, day: Int) -> Date? {
        dateFormatter.date(from: "\(year)-\(month)-\(day)")
    }
    
    func getYearMonthDayFromDate(date: Date) -> (year: Int, month: Int, day: Int) {
        let dateComponents = Calendar.current.dateComponents(in: TimeZone.current, from: date)
        
        let year = dateComponents.year ?? 2010
        let month = dateComponents.month ?? 1
        let day = dateComponents.day ?? 1
        
        
        return (year, month, day)
    }
    
    @IBAction func autoPlay(_ sender: UISwitch) {
        playLabel.textColor = sender.isOn ? .white : .lightGray
        
        if sender.isOn {
            currentYear = Int(yearSlider.minimumValue)
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(playAction), userInfo: nil, repeats: true)
        }
        else {
            timer.invalidate()
        }
    }
    
    @IBAction func slide(_ sender: UISlider) {
        currentYear = Int(sender.value)
        updateYearView(year: currentYear)
        updatePhotoView(year: currentYear)
    }
    
    @IBAction func chooseDay(_ sender: UIDatePicker) {
        currentYear = getYearMonthDayFromDate(date: datePicker.date).year
        updatePhotoView(year: currentYear)
        updateYearView(year: currentYear)
    }
    
    @objc func playAction() {
        updatePhotoView(year: self.currentYear)
        updateYearView(year: self.currentYear)
        currentYear += 1
        
        if (currentYear > Int(yearSlider.maximumValue)) {
            currentYear = Int(yearSlider.minimumValue)
        }
    }
        
    func updatePhotoView(year: Int) {
        currentYearLabel.text = String(year)
        let photo = yearPhotos.getPhotoWithYear(year: year)
        currentImage.image = photo?.image
        descriptionLabel.text = photo?.photoDesc
    }
    
    func updateYearView(year: Int) {
        yearSlider.value = Float(year)
        let currentDate = getYearMonthDayFromDate(date: datePicker.date)
        let currentMonth = currentDate.month
        let currentDay = currentDate.day
        updateDatePicker(year: year, month: currentMonth, day: currentDay)
    }
    
    func updateDatePicker(year: Int, month: Int, day: Int) {
        if let date = convertDateToInstance(year: year, month: month, day: day)
        {
            datePicker.date = date
        }
    }
    
}

